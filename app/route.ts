import {HomeComponent} from 'app/components/home';
import {MangaComponent} from 'app/components/manga';
import {ChapterComponent} from 'app/components/chapter';

export var APP_ROUTE:Array<Object> = [
    {path: '/', name: 'Home', component: HomeComponent},
    {path: '/truyen/:slug', name: 'Manga', component: MangaComponent},
    {path: '/:slug', name: 'Chapter', component: ChapterComponent}
];