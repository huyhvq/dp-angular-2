System.register(['app/components/home', 'app/components/manga', 'app/components/chapter'], function(exports_1) {
    var home_1, manga_1, chapter_1;
    var APP_ROUTE;
    return {
        setters:[
            function (home_1_1) {
                home_1 = home_1_1;
            },
            function (manga_1_1) {
                manga_1 = manga_1_1;
            },
            function (chapter_1_1) {
                chapter_1 = chapter_1_1;
            }],
        execute: function() {
            exports_1("APP_ROUTE", APP_ROUTE = [
                { path: '/', name: 'Home', component: home_1.HomeComponent },
                { path: '/truyen/:slug', name: 'Manga', component: manga_1.MangaComponent },
                { path: '/:slug', name: 'Chapter', component: chapter_1.ChapterComponent }
            ]);
        }
    }
});
//# sourceMappingURL=route.js.map