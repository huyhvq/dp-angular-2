import {Component, OnInit} from 'angular2/core';

import {TEMPLATE_PATH} from 'app/config/template-path';
import {MangaService} from 'app/services/manga';

@Component({
    selector: 'sidebar',
    templateUrl: TEMPLATE_PATH.components.homeRightCol,
    providers: [MangaService]
})

export class HomeRightColComponent implements OnInit {

    public mangas:Array<Object>;

    public newestMangs:Array<Object>;

    constructor(private _mangaService:MangaService) {

    }

    ngOnInit() {
        this.getList();
        this.getNewestManga();
    }

    getList() {
        this._mangaService.getHotestData().then(result => {
            this.mangas = result;
        });
    }

    getNewestManga() {
        this._mangaService.getNewestMangaData().then(result => {
            this.newestMangs = result;
        });
    }

}