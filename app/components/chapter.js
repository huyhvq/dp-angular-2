System.register(['angular2/core', 'angular2/router', 'angular2/common', 'app/config/template-path', 'app/components/jquery-plugins', 'app/services/manga', "angular2/src/facade/lang"], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, common_1, template_path_1, jquery_plugins_1, manga_1, lang_1;
    var ChapterComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (template_path_1_1) {
                template_path_1 = template_path_1_1;
            },
            function (jquery_plugins_1_1) {
                jquery_plugins_1 = jquery_plugins_1_1;
            },
            function (manga_1_1) {
                manga_1 = manga_1_1;
            },
            function (lang_1_1) {
                lang_1 = lang_1_1;
            }],
        execute: function() {
            ChapterComponent = (function () {
                function ChapterComponent(_mangaService, params, _el, _jqp, _route) {
                    this._mangaService = _mangaService;
                    this.params = params;
                    this._el = _el;
                    this._jqp = _jqp;
                    this._route = _route;
                    this.getData();
                }
                ChapterComponent.prototype.getData = function () {
                    var _this = this;
                    this._mangaService.getChapterBySlug(this.params.get('slug')).then(function (result) {
                        _this.images = lang_1.Json.parse(result.content);
                        _this.chapters = result.mangas.chapters;
                        _this.currentChapterSlug = result.slug;
                        _this.currentChapter = {
                            chapter: result.chapter,
                            orderNumber: result.order_number,
                            mangaName: result.mangas.name,
                            mangaSlug: result.mangas.slug,
                            slug: result.slug,
                            nextChapter: result.nextChapter,
                            previousChapter: result.previousChapter
                        };
                    });
                };
                ChapterComponent.prototype.removeEscapeString = function (string) {
                    return string.replace(/\\"/g, '"');
                };
                ChapterComponent.prototype.goToChapter = function () {
                    var _this = this;
                    setTimeout(function () {
                        _this._route.navigate(['Chapter', { slug: _this.currentChapterSlug }]);
                    });
                };
                ChapterComponent.prototype.nextChapter = function () {
                    if (this.currentChapter.nextChapter != null) {
                        this._route.navigate(['Chapter', { slug: this.currentChapter.nextChapter.slug }]);
                    }
                };
                ChapterComponent.prototype.previousChapter = function () {
                    if (this.currentChapter.previousChapter != null) {
                        this._route.navigate(['Chapter', { slug: this.currentChapter.previousChapter.slug }]);
                    }
                };
                ChapterComponent = __decorate([
                    core_1.Component({
                        templateUrl: template_path_1.TEMPLATE_PATH.components.chapter,
                        directives: [router_1.ROUTER_DIRECTIVES, common_1.FORM_DIRECTIVES],
                        providers: [manga_1.MangaService]
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof manga_1.MangaService !== 'undefined' && manga_1.MangaService) === 'function' && _a) || Object, router_1.RouteParams, core_1.ElementRef, (typeof (_b = typeof jquery_plugins_1.JqueryPlugin !== 'undefined' && jquery_plugins_1.JqueryPlugin) === 'function' && _b) || Object, router_1.Router])
                ], ChapterComponent);
                return ChapterComponent;
                var _a, _b;
            })();
            exports_1("ChapterComponent", ChapterComponent);
        }
    }
});
//# sourceMappingURL=chapter.js.map