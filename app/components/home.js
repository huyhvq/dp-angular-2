System.register(['angular2/core', 'app/config/template-path', 'app/components/home.slider', 'app/components/home.newarrivals', 'app/components/home.rightcol', 'app/services/manga'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, template_path_1, home_slider_1, home_newarrivals_1, home_rightcol_1, manga_1;
    var HomeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (template_path_1_1) {
                template_path_1 = template_path_1_1;
            },
            function (home_slider_1_1) {
                home_slider_1 = home_slider_1_1;
            },
            function (home_newarrivals_1_1) {
                home_newarrivals_1 = home_newarrivals_1_1;
            },
            function (home_rightcol_1_1) {
                home_rightcol_1 = home_rightcol_1_1;
            },
            function (manga_1_1) {
                manga_1 = manga_1_1;
            }],
        execute: function() {
            HomeComponent = (function () {
                function HomeComponent() {
                }
                HomeComponent = __decorate([
                    core_1.Component({
                        templateUrl: template_path_1.TEMPLATE_PATH.components.home,
                        directives: [home_slider_1.HomeSliderComponent, home_newarrivals_1.HomeNewArrivalComponent, home_rightcol_1.HomeRightColComponent],
                        providers: [manga_1.MangaService]
                    }), 
                    __metadata('design:paramtypes', [])
                ], HomeComponent);
                return HomeComponent;
            })();
            exports_1("HomeComponent", HomeComponent);
        }
    }
});
//# sourceMappingURL=home.js.map