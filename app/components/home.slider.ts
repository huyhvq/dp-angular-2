import {Component,ElementRef} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

import {APP_ROUTE} from 'app/route';
import {TEMPLATE_PATH} from 'app/config/template-path';
import {MangaService} from 'app/services/manga';
import {JqueryPlugin} from 'app/components/jquery-plugins';

@Component({
    selector: 'home-slider',
    templateUrl: TEMPLATE_PATH.components.homeSlider,
    providers: [MangaService, JqueryPlugin],
    directives: [ROUTER_DIRECTIVES]
})


export class HomeSliderComponent {

    public mangas:Array<Object>;

    constructor(private _mangaService:MangaService, private _el:ElementRef, private _jqp:JqueryPlugin) {
        this.getList();
    }

    getList() {
        this._mangaService.getSliderData().then(result => {
            this.mangas = result;
            this.setPlugin();
        });
    }

    setPlugin() {
        var sliderEl = this._el.nativeElement;
        this._jqp.setICarousel(sliderEl);
    }

}