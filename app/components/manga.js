System.register(['angular2/core', 'angular2/router', 'app/config/template-path', 'app/components/home.rightcol', 'app/components/jquery-plugins', 'app/services/manga'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, template_path_1, home_rightcol_1, jquery_plugins_1, manga_1;
    var MangaComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (template_path_1_1) {
                template_path_1 = template_path_1_1;
            },
            function (home_rightcol_1_1) {
                home_rightcol_1 = home_rightcol_1_1;
            },
            function (jquery_plugins_1_1) {
                jquery_plugins_1 = jquery_plugins_1_1;
            },
            function (manga_1_1) {
                manga_1 = manga_1_1;
            }],
        execute: function() {
            MangaComponent = (function () {
                function MangaComponent(_mangaService, params, _el, _jqp) {
                    this._mangaService = _mangaService;
                    this.params = params;
                    this._el = _el;
                    this._jqp = _jqp;
                    this.getData();
                }
                MangaComponent.prototype.getData = function () {
                    var _this = this;
                    this._mangaService.getMangaBySlug(this.params.get('slug')).then(function (result) {
                        _this.manga = result;
                        _this.chapters = result.chapters;
                        _this._jqp.setPerfectScrollbar(_this._el.nativeElement);
                    });
                };
                MangaComponent.prototype.convertDate = function (date) {
                    moment.locale('vi_vn');
                    return moment(date).fromNow();
                };
                MangaComponent = __decorate([
                    core_1.Component({
                        templateUrl: template_path_1.TEMPLATE_PATH.components.manga,
                        directives: [home_rightcol_1.HomeRightColComponent, router_1.ROUTER_DIRECTIVES],
                        providers: [manga_1.MangaService]
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof manga_1.MangaService !== 'undefined' && manga_1.MangaService) === 'function' && _a) || Object, router_1.RouteParams, core_1.ElementRef, (typeof (_b = typeof jquery_plugins_1.JqueryPlugin !== 'undefined' && jquery_plugins_1.JqueryPlugin) === 'function' && _b) || Object])
                ], MangaComponent);
                return MangaComponent;
                var _a, _b;
            })();
            exports_1("MangaComponent", MangaComponent);
        }
    }
});
//# sourceMappingURL=manga.js.map