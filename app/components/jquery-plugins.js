System.register([], function(exports_1) {
    var JqueryPlugin;
    return {
        setters:[],
        execute: function() {
            JqueryPlugin = (function () {
                function JqueryPlugin() {
                }
                JqueryPlugin.prototype.moveTop = function () {
                    jQuery('html, body').animate({ scrollTop: 0 }, 500);
                };
                JqueryPlugin.prototype.setICarousel = function (el) {
                    setTimeout(function () {
                        jQuery(el).find('#icarousel').iCarousel({});
                    });
                };
                JqueryPlugin.prototype.setPerfectScrollbar = function (el) {
                    setTimeout(function () {
                        jQuery(el).find('.list-chapter-wrp').perfectScrollbar();
                    });
                };
                return JqueryPlugin;
            })();
            exports_1("JqueryPlugin", JqueryPlugin);
        }
    }
});
//# sourceMappingURL=jquery-plugins.js.map