System.register(['angular2/core', 'angular2/router', 'app/route', 'app/config/template-path', 'app/components/jquery-plugins'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, route_1, template_path_1, jquery_plugins_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (route_1_1) {
                route_1 = route_1_1;
            },
            function (template_path_1_1) {
                template_path_1 = template_path_1_1;
            },
            function (jquery_plugins_1_1) {
                jquery_plugins_1 = jquery_plugins_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(_jpq) {
                    this._jpq = _jpq;
                }
                AppComponent.prototype.moveTop = function () {
                    this._jpq.moveTop();
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: template_path_1.TEMPLATE_PATH.components.app,
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [jquery_plugins_1.JqueryPlugin]
                    }),
                    router_1.RouteConfig(route_1.APP_ROUTE), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof jquery_plugins_1.JqueryPlugin !== 'undefined' && jquery_plugins_1.JqueryPlugin) === 'function' && _a) || Object])
                ], AppComponent);
                return AppComponent;
                var _a;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.js.map