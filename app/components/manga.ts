import {Component, ElementRef} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams} from 'angular2/router';

import {TEMPLATE_PATH} from 'app/config/template-path';

//import {HomeSliderComponent} from 'app/components/home.slider';
//import {HomeNewArrivalComponent} from 'app/components/home.newarrivals';
import {HomeRightColComponent} from 'app/components/home.rightcol';
import {JqueryPlugin} from 'app/components/jquery-plugins';

import {MangaService} from 'app/services/manga';

@Component({
    templateUrl: TEMPLATE_PATH.components.manga,
    directives: [HomeRightColComponent, ROUTER_DIRECTIVES],
    providers: [MangaService]
})
export class MangaComponent {
    manga;
    chapter;

    constructor(private _mangaService:MangaService, private params:RouteParams, private _el:ElementRef,private _jqp: JqueryPlugin) {
        this.getData();
    }

    getData() {
        this._mangaService.getMangaBySlug(this.params.get('slug')).then(result => {
            this.manga = result;
            this.chapters = result.chapters;
            this._jqp.setPerfectScrollbar(this._el.nativeElement);
        });
    }

    convertDate(date) {
        moment.locale('vi_vn');
        return moment(date).fromNow();
    }
}