import {ElementRef} from 'angular2/core';

export class JqueryPlugin {

    moveTop() {
        jQuery('html, body').animate({scrollTop: 0}, 500);
    }

    setICarousel(el:ElementRef) {
        setTimeout(()=> {
            jQuery(el).find('#icarousel').iCarousel({});
        });
    }

    setPerfectScrollbar(el:ElementRef){
        setTimeout(()=> {
            jQuery(el).find('.list-chapter-wrp').perfectScrollbar();
        });
    }
}