System.register(['angular2/core', 'app/config/template-path', 'app/services/manga'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, template_path_1, manga_1;
    var HomeRightColComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (template_path_1_1) {
                template_path_1 = template_path_1_1;
            },
            function (manga_1_1) {
                manga_1 = manga_1_1;
            }],
        execute: function() {
            HomeRightColComponent = (function () {
                function HomeRightColComponent(_mangaService) {
                    this._mangaService = _mangaService;
                }
                HomeRightColComponent.prototype.ngOnInit = function () {
                    this.getList();
                    this.getNewestManga();
                };
                HomeRightColComponent.prototype.getList = function () {
                    var _this = this;
                    this._mangaService.getHotestData().then(function (result) {
                        _this.mangas = result;
                    });
                };
                HomeRightColComponent.prototype.getNewestManga = function () {
                    var _this = this;
                    this._mangaService.getNewestMangaData().then(function (result) {
                        _this.newestMangs = result;
                    });
                };
                HomeRightColComponent = __decorate([
                    core_1.Component({
                        selector: 'sidebar',
                        templateUrl: template_path_1.TEMPLATE_PATH.components.homeRightCol,
                        providers: [manga_1.MangaService]
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof manga_1.MangaService !== 'undefined' && manga_1.MangaService) === 'function' && _a) || Object])
                ], HomeRightColComponent);
                return HomeRightColComponent;
                var _a;
            })();
            exports_1("HomeRightColComponent", HomeRightColComponent);
        }
    }
});
//# sourceMappingURL=home.rightcol.js.map