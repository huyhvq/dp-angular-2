import {Component, OnInit} from 'angular2/core';

import {TEMPLATE_PATH} from 'app/config/template-path';

import {HomeSliderComponent} from 'app/components/home.slider';
import {HomeNewArrivalComponent} from 'app/components/home.newarrivals';
import {HomeRightColComponent} from 'app/components/home.rightcol';

import {MangaService} from 'app/services/manga';

@Component({
    templateUrl: TEMPLATE_PATH.components.home,
    directives: [HomeSliderComponent, HomeNewArrivalComponent, HomeRightColComponent],
    providers: [MangaService]
})
export class HomeComponent implements OnInit {

}