import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {APP_ROUTE} from 'app/route';
import {TEMPLATE_PATH} from 'app/config/template-path';

import {JqueryPlugin} from 'app/components/jquery-plugins';

@Component({
    selector: 'my-app',
    templateUrl: TEMPLATE_PATH.components.app,
    directives: [ROUTER_DIRECTIVES],
    providers: [JqueryPlugin]
})

@RouteConfig(APP_ROUTE)

export class AppComponent {

    constructor(private _jpq: JqueryPlugin){

    }

    moveTop(){
        this._jpq.moveTop();
    }
}