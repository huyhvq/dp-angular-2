import {Component, OnChanges} from 'angular2/core';

import {TEMPLATE_PATH} from 'app/config/template-path';
import {MangaService} from 'app/services/manga';
import {FORM_DIRECTIVES} from 'angular2/common';

@Component({
    selector: 'new-arrivals',
    templateUrl: TEMPLATE_PATH.components.homeNewArrivals,
    providers: [MangaService],
    directives: [FORM_DIRECTIVES],
    inputs: ['queryResult']
})

export class HomeNewArrivalComponent implements OnChanges {

    public mangas:Array<Object>;
    public queryResult:Object;
    public paginationList:Array;
    public loading:Boolean;

    constructor(private _mangaService:MangaService) {
        this.loading = true;
        this.getList();
    }

    getList() {
        this._mangaService.getLatestData(1).then(result => {
            this.queryResult = result;
            this.mangas = this.queryResult.data;
            this.renderPagination();
            this.loading = false;
        });

    }

    convertDate(date) {
        moment.locale('vi_vn');
        return moment(date).fromNow();
    }

    renderPagination() {
        this.paginationList = [];
        var start = 1;
        var stop;
        if (this.queryResult.last_page <= 5) {
            stop = this.queryResult.last_page;
        } else {
            if (this.queryResult.current_page - 3 <= 0) {
                stop = this.queryResult.current_page + (5 - this.queryResult.current_page);
            } else {
                start = this.queryResult.current_page - 2;
                stop = this.queryResult.current_page + 2;
                if (stop > this.queryResult.last_page) {
                    start = this.queryResult.last_page - 4;
                    stop = this.queryResult.last_page;
                }
            }
        }
        for (var i = start; i <= stop; i++) {
            this.paginationList.push(i);
        }

    }

    goToPage(page) {
        if (page <= 0 || page > this.queryResult.last_page) {
            return false;
        } else {
            this.loading = true;
            this._mangaService.getLatestData(page).then(result => {
                this.queryResult = result;
                this.mangas = this.queryResult.data;
                this.renderPagination();
                jQuery('html, body').animate({scrollTop: 699}, 500);
                this.loading = false;
            });
        }
    }

    prePage() {
        this.goToPage(this.queryResult.current_page - 1);
    }

    nextPage() {
        this.goToPage(this.queryResult.current_page + 1);
    }

}