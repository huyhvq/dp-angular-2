import {Component, ElementRef} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteParams} from 'angular2/router';
import {FORM_DIRECTIVES} from 'angular2/common';

import {TEMPLATE_PATH} from 'app/config/template-path';

import {JqueryPlugin} from 'app/components/jquery-plugins';

import {MangaService} from 'app/services/manga';
import {Json} from "angular2/src/facade/lang";

@Component({
    templateUrl: TEMPLATE_PATH.components.chapter,
    directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES],
    providers: [MangaService]
})
export class ChapterComponent {
    images;
    manga;
    currentChapter;
    chapters;
    currentChapterSlug;

    constructor(private _mangaService:MangaService, private params:RouteParams, private _el:ElementRef, private _jqp:JqueryPlugin, private _route:Router) {
        this.getData();
    }

    getData() {
        this._mangaService.getChapterBySlug(this.params.get('slug')).then(result => {
            this.images = Json.parse(result.content);
            this.chapters = result.mangas.chapters;
            this.currentChapterSlug = result.slug;
            this.currentChapter = {
                chapter: result.chapter,
                orderNumber: result.order_number,
                mangaName: result.mangas.name,
                mangaSlug: result.mangas.slug,
                slug: result.slug,
                nextChapter: result.nextChapter,
                previousChapter: result.previousChapter
            };

        });
    }

    removeEscapeString(string) {
        return string.replace(/\\"/g, '"');
    }

    goToChapter() {
        setTimeout(() => {
            this._route.navigate(['Chapter', {slug: this.currentChapterSlug}]);
        });
    }

    nextChapter() {
        if (this.currentChapter.nextChapter != null) {
            this._route.navigate(['Chapter', {slug: this.currentChapter.nextChapter.slug}]);
        }
    }

    previousChapter() {
        if (this.currentChapter.previousChapter != null) {
            this._route.navigate(['Chapter', {slug: this.currentChapter.previousChapter.slug}]);
        }
    }

}