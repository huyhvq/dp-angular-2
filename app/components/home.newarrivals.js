System.register(['angular2/core', 'app/config/template-path', 'app/services/manga', 'angular2/common'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, template_path_1, manga_1, common_1;
    var HomeNewArrivalComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (template_path_1_1) {
                template_path_1 = template_path_1_1;
            },
            function (manga_1_1) {
                manga_1 = manga_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            }],
        execute: function() {
            HomeNewArrivalComponent = (function () {
                function HomeNewArrivalComponent(_mangaService) {
                    this._mangaService = _mangaService;
                    this.loading = true;
                    this.getList();
                }
                HomeNewArrivalComponent.prototype.getList = function () {
                    var _this = this;
                    this._mangaService.getLatestData(1).then(function (result) {
                        _this.queryResult = result;
                        _this.mangas = _this.queryResult.data;
                        _this.renderPagination();
                        _this.loading = false;
                    });
                };
                HomeNewArrivalComponent.prototype.convertDate = function (date) {
                    moment.locale('vi_vn');
                    return moment(date).fromNow();
                };
                HomeNewArrivalComponent.prototype.renderPagination = function () {
                    this.paginationList = [];
                    var start = 1;
                    var stop;
                    if (this.queryResult.last_page <= 5) {
                        stop = this.queryResult.last_page;
                    }
                    else {
                        if (this.queryResult.current_page - 3 <= 0) {
                            stop = this.queryResult.current_page + (5 - this.queryResult.current_page);
                        }
                        else {
                            start = this.queryResult.current_page - 2;
                            stop = this.queryResult.current_page + 2;
                            if (stop > this.queryResult.last_page) {
                                start = this.queryResult.last_page - 4;
                                stop = this.queryResult.last_page;
                            }
                        }
                    }
                    for (var i = start; i <= stop; i++) {
                        this.paginationList.push(i);
                    }
                };
                HomeNewArrivalComponent.prototype.goToPage = function (page) {
                    var _this = this;
                    if (page <= 0 || page > this.queryResult.last_page) {
                        return false;
                    }
                    else {
                        this.loading = true;
                        this._mangaService.getLatestData(page).then(function (result) {
                            _this.queryResult = result;
                            _this.mangas = _this.queryResult.data;
                            _this.renderPagination();
                            jQuery('html, body').animate({ scrollTop: 699 }, 500);
                            _this.loading = false;
                        });
                    }
                };
                HomeNewArrivalComponent.prototype.prePage = function () {
                    this.goToPage(this.queryResult.current_page - 1);
                };
                HomeNewArrivalComponent.prototype.nextPage = function () {
                    this.goToPage(this.queryResult.current_page + 1);
                };
                HomeNewArrivalComponent = __decorate([
                    core_1.Component({
                        selector: 'new-arrivals',
                        templateUrl: template_path_1.TEMPLATE_PATH.components.homeNewArrivals,
                        providers: [manga_1.MangaService],
                        directives: [common_1.FORM_DIRECTIVES],
                        inputs: ['queryResult']
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof manga_1.MangaService !== 'undefined' && manga_1.MangaService) === 'function' && _a) || Object])
                ], HomeNewArrivalComponent);
                return HomeNewArrivalComponent;
                var _a;
            })();
            exports_1("HomeNewArrivalComponent", HomeNewArrivalComponent);
        }
    }
});
//# sourceMappingURL=home.newarrivals.js.map