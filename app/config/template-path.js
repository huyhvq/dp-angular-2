System.register([], function(exports_1) {
    var TEMPLATE_PATH;
    return {
        setters:[],
        execute: function() {
            exports_1("TEMPLATE_PATH", TEMPLATE_PATH = {
                components: {
                    app: 'resources/views/app.html',
                    home: 'resources/views/home.html',
                    homeSlider: 'resources/views/home.slider.html',
                    homeNewArrivals: 'resources/views/home.new-arrivals.html',
                    homeRightCol: 'resources/views/home.rightcol.html',
                    manga: 'resources/views/manga.html',
                    chapter: 'resources/views/chapter.html',
                }
            });
        }
    }
});
//# sourceMappingURL=template-path.js.map