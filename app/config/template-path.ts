export var TEMPLATE_PATH:Object = {
    components: {
        app: 'resources/views/app.html',
        home: 'resources/views/home.html',
        homeSlider: 'resources/views/home.slider.html',
        homeNewArrivals: 'resources/views/home.new-arrivals.html',
        homeRightCol: 'resources/views/home.rightcol.html',
        manga: 'resources/views/manga.html',
        chapter: 'resources/views/chapter.html',
    }
};