import {Http} from 'angular2/http';
import {Injectable} from "angular2/core";

@Injectable()
export class MangaService {

    private _endpoint = 'http://lumen.app/';

    private _sliderEndpoint = this._endpoint.concat('slider-manga.json');
    private _homePageEndpoint = this._endpoint.concat('home-data.json');

    private _latestEndpoint = this._endpoint.concat('new-arrival-test.json?page=');
    private _hotestEndpoint = this._endpoint.concat('hotest-test.json');
    private _newestMangaEndpoint = this._endpoint.concat('newest-manga.json');
    private _mangaBySlug = this._endpoint.concat('manga/');
    private _chapterBySlug = this._endpoint.concat('chapter/');

    constructor(private _http:Http) {

    }

    fetch(endPoint) {
        return new Promise(resolve => {
            this._http.get(endPoint).subscribe(data => {
                resolve(data.json());
            })
        });
    }

    getSliderData() {
        return this.fetch(this._sliderEndpoint);
    }

    getLatestData(page) {
        return this.fetch(this._latestEndpoint.concat(page));
    }

    getHotestData() {
        return this.fetch(this._hotestEndpoint);
    }

    getHomePageData() {
        return this.fetch(this._homePageEndpoint);
    }

    getNewestMangaData() {
        return this.fetch(this._newestMangaEndpoint);
    }

    getMangaBySlug(slug) {
        return this.fetch(this._mangaBySlug.concat(slug));
    }

    getChapterBySlug(slug) {
        return this.fetch(this._chapterBySlug.concat(slug));
    }
}