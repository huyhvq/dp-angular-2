System.register(['angular2/http', "angular2/core"], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1;
    var MangaService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            MangaService = (function () {
                function MangaService(_http) {
                    this._http = _http;
                    this._endpoint = 'http://lumen.app/';
                    this._sliderEndpoint = this._endpoint.concat('slider-manga.json');
                    this._homePageEndpoint = this._endpoint.concat('home-data.json');
                    this._latestEndpoint = this._endpoint.concat('new-arrival-test.json?page=');
                    this._hotestEndpoint = this._endpoint.concat('hotest-test.json');
                    this._newestMangaEndpoint = this._endpoint.concat('newest-manga.json');
                    this._mangaBySlug = this._endpoint.concat('manga/');
                    this._chapterBySlug = this._endpoint.concat('chapter/');
                }
                MangaService.prototype.fetch = function (endPoint) {
                    var _this = this;
                    return new Promise(function (resolve) {
                        _this._http.get(endPoint).subscribe(function (data) {
                            resolve(data.json());
                        });
                    });
                };
                MangaService.prototype.getSliderData = function () {
                    return this.fetch(this._sliderEndpoint);
                };
                MangaService.prototype.getLatestData = function (page) {
                    return this.fetch(this._latestEndpoint.concat(page));
                };
                MangaService.prototype.getHotestData = function () {
                    return this.fetch(this._hotestEndpoint);
                };
                MangaService.prototype.getHomePageData = function () {
                    return this.fetch(this._homePageEndpoint);
                };
                MangaService.prototype.getNewestMangaData = function () {
                    return this.fetch(this._newestMangaEndpoint);
                };
                MangaService.prototype.getMangaBySlug = function (slug) {
                    return this.fetch(this._mangaBySlug.concat(slug));
                };
                MangaService.prototype.getChapterBySlug = function (slug) {
                    return this.fetch(this._chapterBySlug.concat(slug));
                };
                MangaService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], MangaService);
                return MangaService;
            })();
            exports_1("MangaService", MangaService);
        }
    }
});
//# sourceMappingURL=manga.js.map