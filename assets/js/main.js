$(document).ready(function () {
    $('#icarousel').iCarousel({
        mouseWheel: false,
        keyboardNav: false,
        animationSpeed: 800,
        pauseTime: 10000,
        pauseOnHover: true,
        perspective: 0,
        make3D: true,
        autoPlay: false
    });
    $('.move-top').click(function() {
		$('html, body').animate({scrollTop:0},500);
	});
    $('.mobile-toggle').click(function(){
        $('.mobile-wrp').fadeIn(200);
    });
    $('.mobile-close').click(function(){
        $('.mobile-wrp').hide();
    });
    $('.open-top-form').click(function(e){
        $('.top-form').hide();
        e.preventDefault();
        var rel = $(this).attr('rel');
        $('.'+rel).fadeIn(200);
    });
    $('.top-form-close').click(function(){
        $('.top-form').hide();
    });
});
$(document).scroll(function(){
    
});

$(function() {
    $('.list-chapter-wrp').perfectScrollbar();
//    $('.staff-right-story').perfectScrollbar();
});
var flag = false;
$(window).load(function(){
    var ww = $(window).innerWidth();
    var wh = $(window).height();
    var heading = '<div class="home-latest-head-list box-list-head-list box-list-head-list-mobile"><h4 class="box-list-head-list-1">Truyện</h4><h4 class="box-list-head-list-2">Tác giả</h4><h4 class="box-list-head-list-3">Nhóm dịch</h4><h4 class="box-list-head-list-4">Đánh giá</h4><h4 class="box-list-head-list-5">Lượt xem</h4><h4 class="box-list-head-list-6">Cập nhật lần cuối</h4><div class="clear"></div></div>';
    var below = '<div class="spacer10"></div>';

    if(ww < 1196){
        if(flag == false){
            $('.box-list-comic').before(heading);
            $('.box-list-comic').after(below);
            flag = true;
        }
    }
});
$(window).resize(function(){
    var ww = $(window).innerWidth();
    var wh = $(window).height();

    var heading = '<div class="home-latest-head-list box-list-head-list box-list-head-list-mobile"><h4 class="box-list-head-list-1">Truyện</h4><h4 class="box-list-head-list-2">Tác giả</h4><h4 class="box-list-head-list-3">Nhóm dịch</h4><h4 class="box-list-head-list-4">Đánh giá</h4><h4 class="box-list-head-list-5">Lượt xem</h4><h4 class="box-list-head-list-6">Cập nhật lần cuối</h4><div class="clear"></div></div>';
    var below = '<div class="spacer10 box-list-bottom-list-mobile"></div>';
    if(ww < 1196){
        if(flag == false){
            flag = true;
            $('.box-list-comic').before(heading);
            $('.box-list-comic').after(below);
        }
    }else{
        $('.box-list-head-list-mobile, .box-list-bottom-list-mobile').hide();
        flag = false;
    }
});